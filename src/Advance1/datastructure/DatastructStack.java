package Advance1.datastructure;
import java.util.*;

public class DatastructStack {
    public static void main(String[] args) {
        //    Cấu trúc dữ liệu
        //    Ngăn xếp: Last In First Out (an abstract data structure)
//		      -----------------
//            |	   top		|
//            -----------------
//            |				|
//            -----------------
//            |	 bottom		|
//            -----------------
//      + Push: thêm vào top
//		+ Pop:  lấy ra từ top
//		+ Peek: xem thằng top có gì

        Stack<Integer> st = new Stack<>();
        // hàm truyền vào ngăn xếp
        st.push(1);
        st.push(2);
        st.push(3);
        st.push(4);
        st.push(5);
        st.push(6);
        st.push(7);
        System.out.println(st);
        // tìm kiếm phần tử
        System.out.println(st.search(5));
        // hàm lấy ra từ top
        st.pop();
        System.out.println(st);
        // hàm peek: nhìn vào thằng top nó là cái j
        System.out.println(st.peek());
        //hàm kiểm tra stạck có trống hay không
        System.out.println(st.empty());
        // sửa phần tử thứ (?1) thành (?2) (?1,?2)
        st.set(3,9);
        System.out.println(st);
        // xóa phần tử thứ ?
        st.remove(2);
        System.out.println(st);


    }
}
