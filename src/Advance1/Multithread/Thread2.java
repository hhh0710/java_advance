package Advance1.Multithread;

public class Thread2 extends Thread{

    SharedData sharedData;

    public Thread2(SharedData sharedData) {
        this.sharedData = sharedData;
    }

    @Override
    public void run() {
        while (true){

            synchronized (sharedData){
                try {
                    sharedData.notifyAll();
                    sharedData.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                int result = sharedData.getRannum();
                result *= result;
                System.out.println("result: "+ result);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
            synchronized (sharedData){
                sharedData.notifyAll();
            }

        }
    }
}
