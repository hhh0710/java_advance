package Advance1.Multithread;

public class Thread1 extends Thread{

    SharedData sharedData;

    public Thread1(SharedData sharedData) {
        this.sharedData  =  sharedData;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        while (true){
            synchronized (sharedData){
                int rannum = (int)(Math.random()*20+1);
                sharedData.setRannum(rannum);
                System.out.println("rannum: "+ rannum);

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                sharedData.notifyAll();
                try {
                    sharedData.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


        }
    }
}
