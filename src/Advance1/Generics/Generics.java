package Advance1.Generics;

import org.jetbrains.annotations.NotNull;

public class Generics {
        public static <T> void printArray(T @NotNull [] elements) {
            for (T element : elements) {
                System.out.print(element + " ");
            }
            System.out.println();
        }

        public static void main(String args[]) {

            Integer[] intArray = {0,1,2,3,4,5,6};
            Character[] charArray = { 'J', 'A', 'V', 'A' };

            System.out.print("In mảng số nguyên: ");
            printArray(intArray);

            System.out.print("In mảng ký tự: ");
            printArray(charArray);
        }
}
