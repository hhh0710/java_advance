package Advance1.Collection;
import java.util.*;
public class Set_Interface {
        public static void main(String[] args) {

            Set setC = new LinkedHashSet();
            Set setD = new TreeSet();

            Set<String> setA = new HashSet<String>();
            setA.add("Java");
            setA.add("Python");
            setA.add("Java");
            setA.add("PHP");
            System.out.println("Số phần tử của setA: " + setA.size());
            System.out.println("Các phần tử của setA: " + setA);
            System.out.println("setA có chứa Java không? " + setA.contains("Java"));
            System.out.println("setA có chứa C++ không? " + setA.contains("C++"));

//            enum days {
//                SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
//            }
//            Set<String> setB = new EnumSet();
//            setB.add("Java");
//            setB.add("Python");
//            setB.add("Java");
//            setB.add("PHP");
//            System.out.println("Số phần tử của setA: " + setB.size());
//            System.out.println("Các phần tử của setA: " + setB);
//            System.out.println("setA có chứa Java không? " + setB.contains("Java"));
//            System.out.println("setA có chứa C++ không? " + setB.contains("C++"));
        //// LinkedHashSet
            // init set object
            Set<String> linkedHashSet = new LinkedHashSet<String>();
            linkedHashSet.add("Java");
            linkedHashSet.add("C++");
            linkedHashSet.add("Java");
            linkedHashSet.add("PHP");
            // show set
            for (String str : linkedHashSet) {
                System.out.println(str);
            }

            // init set
            LinkedHashSet<Student> set = new LinkedHashSet<Student>();
            // create students object
            Student student1 = new Student("Cong", 17, "Hanoi");
            Student student2 = new Student("Dung", 16, "Haiphong");
            Student student3 = new Student("Ngon", 18, "Hanoi");
            Student student4 = new Student("Hanh", 19, "Danang");
            // add students object to set
            set.add(student1);
            set.add(student2);
            set.add(student3);
            set.add(student4);
            set.add(student1);
            // show set
            Iterator<Student> iterator = set.iterator();
            Student student;
            while (iterator.hasNext()) {
                student = iterator.next();
                System.out.println(student.toString());
            }
        //// TreeSet
            // init treeSet object
            TreeSet<String> treeSet1 = new TreeSet<String>();
            treeSet1.add("Java");
            treeSet1.add("C++");
            treeSet1.add("Java");
            treeSet1.add("PHP");
            // show treeSet
            for (String str : treeSet1) {
                System.out.println(str);
            }
            // init treeSet
            TreeSet<Student> treeSet = new TreeSet<>();
            // create students object
            Student student5 = new Student("hoang", 17, "Hanoi");
            Student student6 = new Student("toan", 16, "Haiphong");
            Student student7 = new Student("thao", 18, "Hanoi");
            Student student8 = new Student("thu", 19, "Danang");
            // add students object to treeSet
            treeSet.add(student5);
            treeSet.add(student6);
            treeSet.add(student7);
            treeSet.add(student8);
            treeSet.add(student5);
            // show treeSet
            for (Student students : treeSet) {
                System.out.println(students.toString());
            }
        }
}
class Student {
    private String name;
    private int age;
    private String address;

    public Student() {
    }

    public Student(String name, int age, String address) {
        super();
        this.name = name;
        this.age = age;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Student@name=" + name + ",age=" + age + ",address=" + address;
    }
}
class Student2 implements Comparable<Student> {
    private String name;
    private int age;
    private String address;

    public Student2() {
    }

    public Student2(String name, int age, String address) {
        super();
        this.name = name;
        this.age = age;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Student@name=" + name + ",age=" + age + ",address=" + address;
    }

    @Override
    public int compareTo(Student student) {
        // sort student's name by ASC
        return this.getName().compareTo(student.getName());
    }
}
