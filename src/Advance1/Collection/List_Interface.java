
package Advance1.Collection;

import java.util.*;
import java.util.ListIterator;
public class List_Interface {

    public static void main(String[] args) {
//        //ArrayList:
//
//        ArrayList<String> arl = new ArrayList<>();
//        //add
//        arl.add("chó");
//        arl.add("mèo");
//        arl.add("lợn");
//        arl.add("gà");
//        arl.add("vịt");
//        //in ra ArrayList
//        System.out.println(arl);
//        // in ra phần tử trong ArrayList
//        for(int i=0;i< arl.size();i++){
//            System.out.println(arl.get(i));
//        }
//
//        //remove
//        arl.remove(0);
//        System.out.println(arl);
//        //get(xem vị trí thứ 1)
//        System.out.println(arl.get(1));
//        // thay vị trí ? bằng ?
//        arl.set(3,"bò");
//        System.out.println(arl);
//        //clear
//        arl.clear();
//        System.out.println(arl);
//
//        //Vector
//        Vector<String> vec = new Vector<>();
//        vec.add("dell");
//        vec.add("lenovo");
//        vec.add("asus");
//        vec.add("hp");
//        vec.add("acer");
//        //in ra Vector
//        System.out.println(vec);
//        // in ra phần tử trong Vector
//        for(int i=0;i< vec.size();i++){
//            System.out.println(vec.get(i));
//        }
//
//        //remove
//        vec.remove(0);
//        System.out.println(vec);
//        //get(xem vị trí thứ 1)
//        System.out.println(vec.get(1));
//        // thay vị trí ? bằng ?
//        vec.set(3,"bò");
//        System.out.println(vec);
//        //clear
//        vec.clear();
//        System.out.println(vec);
//
//        // y hịt Arraylist
//        // khác phần mảng khởi tạo ra ban đầu có 10 phần tử
//
//        //LinkedList
//        LinkedList<String> Llist = new LinkedList<>();
//        Llist.add("php");
//        Llist.add("java");
//        Llist.add("c++");
//        Llist.add("javascript");
//        Llist.add("python");
//        System.out.println(Llist);
//        //1 số cách duyệt các phần tử
//        // c1: use iterator
//        Iterator<String> Itr =  Llist.iterator();
//        while (Itr.hasNext()){
//            System.out.print(Itr.next()+",");
//            System.out.println();
//        }
//
//
//        System.out.println();
//        System.out.println("----------------------------------");
//        //c2: use foreach:
//        for(String objName: Llist){
//            System.out.print(objName + ",");
//        }
//        System.out.println();
//        System.out.println("----------------------------------");
//        //c3: use index:
//        for(int i=0;i< Llist.size(); i++){
//            System.out.print(Llist.get(i)+",");
//        }


        //List
        List<String> list = new ArrayList<>();
        list.add("php");
        list.add("java");
        list.add("c++");
        list.add("javascript");
        list.add("python");
        System.out.println(list);
        ListIterator<String> itr =  list.listIterator();
        System.out.println("Duyet cac phan tu tu dau den cuoi:");
        while (itr.hasNext()) {
            System.out.println("\t" + itr.next());
        }
        System.out.println("Duyet cac phan tu tu cuoi ve dau:");
        while (itr.hasPrevious()) {
            System.out.println("\t" + itr.previous());
        }
        //

    }



}
