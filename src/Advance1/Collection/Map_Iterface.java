package Advance1.Collection;
import java.util.*;
public class Map_Iterface {
    public static void main(String[] args) {
        // init HashMap
        Map<Integer, String> map = new HashMap<>();
        map.put(100, "A");
        map.put(101, "B");
        map.put(102, "C");
        // show map
        Set<Integer> set = map.keySet();
        for (Integer key : set) {
            System.out.println(key + " " + map.get(key));
        }
        System.out.println(map);
    }
}
