package Advance2.JavaNetwork;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client1Demo {
    public static void main(String[] args) {
        try {
            System.out.println("đang kết nối");
            // connect
            Socket socket = new Socket("localhost", 8888);
            System.out.println("đã kết nối.");
            // hàm đọc ghi dữ liệu
            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            Scanner scanner = new Scanner(System.in);
            while (true) {
                // đẩy dữ liệu lên server bằng hàm ghi
                String str2 = scanner.nextLine();
                dataOutputStream.writeUTF(str2);
                dataOutputStream.flush();
                if (str2.equals("exit")) {
                    break;
                }
                // đọc dữ liệu trả về
                String str = dataInputStream.readUTF();
                System.out.println("server say: " + str);
            }
            dataOutputStream.close();
            dataInputStream.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
