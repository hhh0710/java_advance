package Advance2.JavaNetwork;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ServerDemo {
    public static void main(String[] args) {
        try {
            System.out.println("khởi tao socket- đang chờ kết nối.......");
            // tạo 1 server socket với cổng là 8888
            ServerSocket serverSocket = new ServerSocket(8888);
            // chờ client connect đến server
            Socket socket = serverSocket.accept();
            System.out.println("đã kết nối");
            // tạo hàm đọc ghi dữ liệu
            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            Scanner scanner = new Scanner(System.in);
            while(true){
                //đọc dữ liệu từ client vào str
                String str = dataInputStream.readUTF();
                if (str.equals("exit")){
                    System.out.println("bye bye");
                    break;
                }
                System.out.println("client say: "+ str);
                //ghi dữ liệu và truyền đến client
                String str2 = scanner.nextLine();
                dataOutputStream.writeUTF(str2);
                dataOutputStream.flush();
            }

            dataOutputStream.close();
            dataInputStream.close();
            socket.close();
            serverSocket.close();
            System.out.println("kết thúc");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
