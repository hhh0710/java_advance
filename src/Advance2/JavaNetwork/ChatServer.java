package Advance2.JavaNetwork;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class ChatServer {

    private int port;
    public static ArrayList<Socket> ListSk;

    public ChatServer(int port) {
        this.port = port;
    }

    public void execute() throws IOException {
        ServerSocket serverSocket = new ServerSocket(port);
        WriteServer writeServer = new WriteServer();
        writeServer.start();
        System.out.println("Server is listenning....");
        while (true) {
            Socket socket = serverSocket.accept();
            System.out.println("Connected with" + socket);
            ChatServer.ListSk.add(socket);
            ReadServer readServer = new ReadServer(socket);
            readServer.start();
        }

    }

    public static void main(String[] args) throws IOException {
        ChatServer.ListSk = new ArrayList<>();
        ChatServer chatServer = new ChatServer(7710);
        chatServer.execute();
    }
}

class ReadServer extends Thread {
    private Socket socketserver;

    public ReadServer(Socket socketserver) {
        this.socketserver = socketserver;
    }

    @Override
    public void run() {
        try {
            DataInputStream dataInputStream = new DataInputStream(socketserver.getInputStream());
            while (true) {
                String message = dataInputStream.readUTF();
                if (message.contains("exit")) {
                    ChatServer.ListSk.remove(socketserver);
                    System.out.println("Disconnected with: " + socketserver);
                    dataInputStream.close();
                    socketserver.close();
                    continue;
                }
                for (Socket item : ChatServer.ListSk) {
                    if (item.getPort() != socketserver.getPort()) {
                        DataOutputStream dataOutputStream = new DataOutputStream(item.getOutputStream());
                        dataOutputStream.writeUTF(message);
                    }
                }
                System.out.println(message);
            }
        } catch (IOException e) {
            try {
                socketserver.close();
            } catch (IOException ex) {
                ex.printStackTrace();
                System.out.println("Co loi xay ra");
            }
        }
    }
}

class WriteServer extends Thread {
    @Override
    public void run() {
        DataOutputStream dataOutputStream = null;
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String message = scanner.nextLine();
            try {
                for (Socket item : ChatServer.ListSk) {
                    dataOutputStream = new DataOutputStream(item.getOutputStream());
                    dataOutputStream.writeUTF("Server: " + message);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}

