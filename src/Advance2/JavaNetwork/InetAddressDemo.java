package Advance2.JavaNetwork;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class InetAddressDemo {
    public static void main(String[] args) throws UnknownHostException {
        // get địa chỉ
        InetAddress inetAddress =  InetAddress.getByName("www.google.com.vn");
        System.out.println(inetAddress.getHostAddress());
        System.out.println(inetAddress.getHostName());
    }
}
