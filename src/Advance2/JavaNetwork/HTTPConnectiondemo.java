package Advance2.JavaNetwork;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HTTPConnectiondemo {
    public static void main(String[] args) throws IOException {
        URL url = new URL("https://www.w3schools.com/");
        // mở liên kết
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        // đọc các trường header Header
        for(int i=1; i< httpURLConnection.getHeaderFields().size();i++){
            System.out.println(httpURLConnection.getHeaderFieldKey(i) + "=" + httpURLConnection.getHeaderField(i));
        }
    }
}
