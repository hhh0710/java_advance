package Advance2.JavaNetwork;

import java.io.IOException;
import java.net.*;

public class UDPServer {
    public static void main(String[] args) throws IOException {
        DatagramSocket datagramSocket = new DatagramSocket();
        //tạo 1 InetAddress có địa chỉ là localhost
        InetAddress inetAddress = InetAddress.getByName("localhost");
        //content send
        String string = "hello client !";
        // tạo đối tượng truyền gói tin (buf, length, ip, port)
        DatagramPacket datagramPacket = new DatagramPacket(string.getBytes(),string.length(), inetAddress,7010);
        datagramSocket.send(datagramPacket);
        datagramSocket.close();
    }
}
