package Advance2.JavaNetwork;

import java.io.IOException;
import java.net.*;

public class UDPClient {
    public static void main(String[] args) throws IOException {
        // tạo đối tượng InetAddress có địa chỉ là localhost
        InetAddress inetAddress = InetAddress.getByName("localhost");
        // tạo đối tượng dataSocket
        DatagramSocket datagramSocket = new DatagramSocket(7010,inetAddress);
        //tạo 1 mảng byte để chứa dữ liệu:
        byte[] buf = new byte[1024];
        // tạo 1 datagraPacket để nhận dữ liệu trả về
        DatagramPacket datagramPacket = new DatagramPacket(buf, buf.length);
        // nhận gói tin từ server trả về
        datagramSocket.receive(datagramPacket);
        //đọc dữ liêu
        String string = new String(datagramPacket.getData(),0,buf.length);
        System.out.println(string);
        datagramSocket.close();
    }
}
