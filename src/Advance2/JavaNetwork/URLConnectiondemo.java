package Advance2.JavaNetwork;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class URLConnectiondemo {
    public static void main(String[] args) throws IOException {
        URL url = new URL("https://www.w3schools.com/");
        URLConnection urlConnection = url.openConnection();
        InputStream is = urlConnection.getInputStream();
        int i = is.read();

        while (i != -1){
            System.out.print((char)i);
            i = is.read();
        }


    }
}
