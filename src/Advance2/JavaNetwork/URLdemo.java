package Advance2.JavaNetwork;

import java.net.MalformedURLException;
import java.net.URL;

public class URLdemo {
    public static void main(String[] args) throws MalformedURLException {
        URL url = new URL("https://gitlab.com/hhh0710/java_advance");
        System.out.println(url.getProtocol());
        System.out.println(url.getPort());
        System.out.println(url.getHost());
        System.out.println(url.getFile());
    }
}
