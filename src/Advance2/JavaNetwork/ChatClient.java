package Advance2.JavaNetwork;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;
import java.util.regex.Pattern;

public class ChatClient {
    private InetAddress host;
    private int port;

    public ChatClient(InetAddress host, int port) {
        this.host = host;
        this.port = port;
    }

    public void execute() throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhap username: ");
        String username = scanner.nextLine();
        while (checkUsername(username)) {
            Socket socket = new Socket(host, port);
            ReadClient readClient = new ReadClient(socket);
            readClient.start();
            WriteClient writeClient = new WriteClient(socket, username);
            writeClient.start();
        }
    }

    public boolean checkUsername(String s) {
        String regex = "^[a-zA-Z0-9]+$";
        if (!Pattern.matches(regex, s)) {
            return false;
        } else {
            return true;
        }
    }


    public static void main(String[] args) throws IOException {
        ChatClient chatClient = new ChatClient(InetAddress.getLocalHost(), 7710);
        chatClient.execute();
    }
}

class ReadClient extends Thread {
    private Socket socketclient;

    public ReadClient(Socket socketclient) {
        this.socketclient = socketclient;
    }

    @Override
    public void run() {
        DataInputStream dataInputStream = null;
        try {
            dataInputStream = new DataInputStream(socketclient.getInputStream());
            while (true) {
                String message = dataInputStream.readUTF();
                System.out.println(message);
            }

        } catch (IOException e) {
            e.printStackTrace();
            try {
                socketclient.close();
            } catch (IOException ex) {
                ex.printStackTrace();
                System.out.println("Co loi xay ra");
            }
        }
    }
}

class WriteClient extends Thread {
    private Socket socketclient;
    private String username;


    public WriteClient(Socket socketclient, String username) {
        this.socketclient = socketclient;
        this.username = username;
    }

    @Override
    public void run() {
        DataOutputStream dataOutputStream = null;
        Scanner scanner = null;
        try {
            dataOutputStream = new DataOutputStream(socketclient.getOutputStream());
            scanner = new Scanner(System.in);
            while (true) {
                String message = scanner.nextLine();
                dataOutputStream.writeUTF(username + ": " + message);
                if (message.equals("exit")) {
                    socketclient.close();
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
            try {
                dataOutputStream.close();
                socketclient.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }
    }
}

